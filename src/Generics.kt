import java.time.LocalDate

//Generic Classes : Type of classes can be anything that they want. Similar to templates in C++
class Login<T>(name : T, password: T){
    init {
        println("Name : $name and Password: $password")
//        showName(name)
    }
//    fun showName(name: Int){
//        println("Int function : $name")
//    }
//    fun showName(name: String){
//        println("String function : $name")
//    }
}

fun main(args: Array<String>) {
    var login = Login<String>("Prabha", "123")
    var login2 = Login<Int>(555, 32)
}
fun main(args: Array<String>) {
    //Collections:

    //Default list is immutable
    var myArrayList = listOf<String>("Me", "James", "Bonny", "Life")
    println(myArrayList)
    println(myArrayList[0])
    println(myArrayList)
    for (items in myArrayList)
        println("Items $items")

    //to create mutable arraylist
    var mutableList = mutableListOf<String>("Me", "James", "Bonny", "Life")
    mutableList[0] = "Paul"
    for (items in mutableList)
        println("Mutable Items $items")

    //HashMap
    var hashMap = hashMapOf(1 to "Prabha", 2 to "Sangeetha")
    println(hashMap.get(1))
    for (key in hashMap.keys)
        println("Hashmap Items ${hashMap.get(key)}")

    var myArray = arrayOf(1,4,6,8)
    myArray[0] = 7
    for (x in myArray)
        println("Array item : $x")


}
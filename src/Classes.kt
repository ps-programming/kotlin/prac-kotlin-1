//Classes
class House(type: String, price: Double, yearBuilt: Int, owner: String) {
    var type: String? = null
    var price: Double? = null
    var yearBuilt: Int? = null
    var owner: String? = null

    init {
        //Initializes all of our properties
        this.type = type
        this.price = price
        this.yearBuilt = yearBuilt
        this.owner = owner
    }

    //Getters and Setters : Are not reccomended in Kotlin
    fun GetType(): String? {
        return this.type
    }

    fun SetType(type: String) {
        this.type = type
    }
}

fun main(args: Array<String>) {
    var myHouse = House("Villa", 23000.0, 2000, "Prabha")
    println(myHouse)    //Gives memory address of the object
    println("Type : ${myHouse.type}")
    var secondHouse = House("6 Bedroom", 45000.5, 1990, "Sangeetha")
    println("Owner of 2nd house : " + secondHouse.owner)
    println(secondHouse.GetType())
    secondHouse.SetType("4 Bedroom")
    println(secondHouse.GetType())
}
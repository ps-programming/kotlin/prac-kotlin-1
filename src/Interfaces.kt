//Interface classes : Classes with methods without definition

interface InputHandler {
    fun clicked()   //All of its implementation depends on how the inherited classes implement it
}

//Classes that inherit an interface should implement all of the methods of interface

class Button : InputHandler {
    override fun clicked() {
        //Implement the method here
        println("Button clicked")
    }
}

class TextView : InputHandler {
    override fun clicked() {
        println("Text View clicked")
    }
}

fun main(args: Array<String>) {
    var button = Button()
    button.clicked()
    var textView = TextView()
    textView.clicked()
}
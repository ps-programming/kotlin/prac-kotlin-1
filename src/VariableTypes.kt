fun main(args: Array<String>) {
    //Types : String, Int, Double, Boolean, Float, Char

    //String
    println("Strings : ")
    var country: String
    country = "India"
    println(country)

    //Int
    println("\n\nIntegers : ")
    var name : String = "Prabha"
    var age = 31
    var myAge: Int = 20
    println(name + " is " + myAge + " years old.")
    println("$name is $myAge years old")

    // ? implies that it can be null otherwise a variable cannot be null
    var num: Int? = null

    //Double
    println("\n\nDoubles : ")
    var doub : Double = 23.54
    println("Here is my double $doub")

    //Boolean
    println("\n\nBooleans : ")
    var truth: Boolean = true
    println("The truth is $truth")

    //Float
    println("\n\nFloats : ")
    var flo: Float = 23.4F  //F or f differentiates double and float values
    println("The float number is $flo")

    //Char
    println("\n\nChars : ")
    var chr: Char = 'A' //F or f differentiates double and float values
    println("The character is: $chr")

}
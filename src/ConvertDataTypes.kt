fun main(args:Array<String>){

    //Converting data types

    var age: Int = 31
    println("Converting data types")
    println(age + 10)
    println(age.toString() + 10)
    println(age.toDouble() + 10)
    println(age.toFloat() + 10)

    var bloodPressure = 89.0
    // Type mismatch if : age = bloodPressure
    age = bloodPressure.toInt()
    println(age)
}
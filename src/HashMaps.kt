fun main(args: Array<String>) {
   //HashMaps are used to store collection of key value pairs
    var hashMap = HashMap<String, String>()
    hashMap.put("name", "Sangeetha")
    hashMap.put("age","19")
    println(hashMap.get("name"))

    var hashMap2 = HashMap<Any, Any>()  //Not reccomended
    hashMap2.put("name", "Prabha")
    hashMap2.put("age",20)
    println(hashMap2.get("age"))

    for (k in hashMap.keys)
        println(hashMap.get(k))

}
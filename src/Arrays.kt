fun main(args: Array<String>) {
    //Arrays are generic types
    var myArray = Array<Int>(5){2}
    myArray[0] = 10
    myArray[3] = 5
    println(myArray)
    println(myArray[0])

    //printing objects
    for (element in myArray){
        println("Items : ${element}")
    }

    //printing items inside array via index
    for(index in 0..myArray.size - 1){
        println("Element :" + myArray[index])
    }
}
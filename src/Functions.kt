fun main(args: Array<String>) {
    add(2, 3)
    var x = subtract(5, 4)
    println("Difference of 5 and 4 is : $x")
}

//Function without return type
fun add(num1: Int, num2: Int) {
    var sum = num1 + num2
    println("Sum is $sum")
}


//Function with return type
/*
    Syntax:
    fun functionName(parameters): returnType{
        statements...
        return returnValue
    }
 */
fun subtract(num1: Int, num2: Int): Int {
    var diff = num1 - num2
    return diff
}


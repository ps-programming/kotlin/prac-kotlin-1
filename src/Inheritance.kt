//Class name is followed by paranthesis if you need primary constructor else just give Class name and open crly braces
open class Animal {
    //As the class has a primary constructor, then while creating another constructor primary constructor must be invoked
    /*constructor(parameters) : this()
      {
          statements...
      }
    */
    var name: String? = null
    var color: String? = null
    private var numLegs: Int? = null

    constructor(name: String, color: String, numLegs: Int)  {
        println("first constructor")
        this.name = name
        this.color = color
        this.numLegs = numLegs
    }

    //Overloading constructor
    constructor(name: String, color: String) {
        this.name = name
        this.color = color
        println("overloaded constructor")
    }
    fun setNumLegs(num : Int){
        this.numLegs = num
    }
    fun getNumLegs():Int{
        return this.numLegs!!
    }
}

//Lion class extends Animal class. When we create classes in Kotlin they are initially final and cannot be inherited from. We need to make it open.
class Lion : Animal {
    constructor(): super("Lion", "brown"){
        super.setNumLegs(4)
        println(super.getNumLegs())
    }
}

fun main(args: Array<String>) {
    var lion = Lion()
    lion.setNumLegs(4)
    println(lion.getNumLegs())
    var animal1 = Animal("Dog","black")
    var animal2 = Animal("Dog","black",4)
}
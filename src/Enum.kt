//Enum Classes : Allows us to represent collection of fixed/static information
enum class Suits{
    HEARTS,
    SPADES,
    DIAMONDS,
    CLUBS
}

enum class Directions{
    NORTH,
    SOUTH,
    EAST,
    WEST
}

fun main(args: Array<String>) {
    var cardPlayer = Suits.DIAMONDS
    if (cardPlayer == Suits.DIAMONDS){
        println("You are doing great!")
    }
    var playerDirections = Directions.NORTH
    if (playerDirections == Directions.NORTH){
        println("NORTH DIrECTED")
    }
}
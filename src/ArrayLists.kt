fun main(args: Array<String>) {
    //ArrayLists are generic types and flexible than arrays
    var myArrayList = ArrayList<String>()
    for (range in 0..5) {
        myArrayList.add("Prabhashankar")
        myArrayList.add("Sangeetha")
    }

    for (i in 0..myArrayList.size-1) {
        println("Item : " + myArrayList.get(i))
    }

    myArrayList.remove("Prabhashankar")

    myArrayList.set(1, "Vadayakshi")

    for (names in myArrayList){
        println("You are : ${names}")
    }

    if (myArrayList.contains("Sangeetha")){
        println("\nVadayakshi found")
    }

    myArrayList.remove("Prabhashankar")
}